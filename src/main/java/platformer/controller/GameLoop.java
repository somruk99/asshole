package platformer.controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import platformer.Main;
import platformer.controller.viewController.HUDViewController;
import platformer.controller.viewController.PauseScreenController;
import platformer.model.Character;
import platformer.model.Pause;
import platformer.model.Score;
import platformer.view.Platform;

import java.io.IOException;
import java.util.ArrayList;

public class GameLoop implements Runnable {

    private Platform platform;
    private int frameRate;
    private float interval;
    private boolean running;
    private boolean gameEnd = false;

    private Pane HUD;
    private HUDViewController hudViewController;
    private Pane pauseScreen;
    public PauseScreenController pauseController;
    private FXMLLoader loader;
    public Pause pause = new Pause();
    private MediaPlayer soundEffectPlayer;

    public GameLoop(Platform platform) {
        this.platform = platform;
        frameRate = 5;
        interval = 1000.0f / frameRate; // 1000 ms = 1 second
        running = true;
        try {
            loader = new FXMLLoader(this.getClass().getResource("/HUDView.fxml"));
            HUD = loader.load();
            hudViewController = loader.getController();
            platform.getChildren().add(HUD);
            loader = new FXMLLoader(this.getClass().getResource("/PauseScreen.fxml"));
            pauseScreen = loader.load();
            pauseController = loader.getController();
            pauseController.setOptions(platform);
            platform.getChildren().add(pauseScreen);
        } catch (IOException e) {
            e.printStackTrace();
        }
        pause.pauseProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue == true) {
                soundEffectPlayer = new MediaPlayer(new Media(this.getClass().getResource("/sfx/pausesfx.wav").toString()));
                soundEffectPlayer.play();
                soundEffectPlayer.setOnEndOfMedia(() -> {
                            soundEffectPlayer.dispose();
                        }
                );
                pauseController.fadeIn();
                Main.setPause(true);
            }
            else {
                pauseController.fadeOut();
                resumeGame();
            }
        });

    }

    public synchronized void resumeGame() {
        Main.setPause(false);
        notify();
    }
    public void stopLoop() {
        if (running) running = false;
    }

    private void update(ArrayList<Character> characterList) {
        if (!Main.isPaused()) {
            for (Character character : characterList) {
                if (platform.getKeys().isPressed(character.getLeftKey()) || platform.getKeys().isPressed(character.getRightKey())) {
                    character.getImageView().tick();
                }

                if (platform.getKeys().isPressed(character.getLeftKey())) {
                    character.setScaleX(-1);
                    character.moveLeft();
                    character.trace();
                }

                if (platform.getKeys().isPressed(character.getRightKey())) {
                    character.setScaleX(1);
                    character.moveRight();
                    character.trace();
                }

                if (!platform.getKeys().isPressed(character.getLeftKey()) && !platform.getKeys().isPressed(character.getRightKey())) {
                    character.stop();
                }

                if (platform.getKeys().isPressed(character.getUpKey())) {
                    character.jump();
                }
            }
        }
    }

    @Override
    public void run() {
        hudViewController.setUpHUD(platform.getCharacterList());
        while (running) {

            float time = System.currentTimeMillis();

            update(platform.getCharacterList());
            updateScore(platform.getScoreList(),platform.getCharacterList());
            updateHealth(platform.getCharacterList());
            if(gameEnd) {
                gameEnd = false;
                hudViewController.reset(platform.getCharacterList());
            }
            time = System.currentTimeMillis() - time;

            if (time < interval) {
                try {
                    Thread.sleep((long) (interval - time));
                } catch (InterruptedException ignore) {
                }
            } else {
                try {
                    Thread.sleep((long) (interval - (interval % time)));
                } catch (InterruptedException ignore) {
                }
            }
        }
    }


    private void updateHealth(ArrayList<Character> charactersList) {
        if (!gameEnd) {
            if (charactersList.get(0).getStats().getHealth() != hudViewController.player1CurrentHealth) {
                hudViewController.rerenderHealth(1, charactersList.get(0).getStats().getHealth());
            }
            if (charactersList.get(1).getStats().getHealth() != hudViewController.player2CurrentHealth) {
                hudViewController.rerenderHealth(2, charactersList.get(1).getStats().getHealth());
            }

            if (charactersList.get(0).getStats().getHealth() == 0) {
                gameEnd = true;
                hudViewController.declareWinner(charactersList,1);
            }
            if (charactersList.get(1).getStats().getHealth() == 0) {
                gameEnd = true;
                hudViewController.declareWinner(charactersList,0);
            }
        }


    }

    private void updateScore(ArrayList<Score> scoreList, ArrayList<Character> characterList) {
        javafx.application.Platform.runLater(() -> {
            for (int i=0 ; i<scoreList.size() ; i++) {
                scoreList.get(i).setPoint(characterList.get(i).getScore());
            }
        });
    }
}
