package platformer.controller.viewController;

import javafx.animation.FadeTransition;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import platformer.Main;
import platformer.view.Platform;
import java.io.IOException;

public class PauseScreenController {

    public AnchorPane pausePane;
    public Label pauseText;
    public Label randomText;
    public Label randomText1;
    public Label randomText2;
    public Label randomText3;
    public Slider volumeSlider;
    public CheckBox mute;

    public KeyCode exitKey = KeyCode.ESCAPE;
    public KeyCode surpiseKey = KeyCode.O;
    public KeyCode backKey = KeyCode.P;

    public String exitText = "Press ["+exitKey.getName()+"] to exit game";
    public String backText = "Press ["+backKey.getName()+"] to back in game";
    public String surpiseText = "Press ["+surpiseKey.getName()+"] if you want to see something";



    public void fadeIn() {
        volumeSlider.setDisable(false);
        FadeTransition fadeIn = new FadeTransition(Duration.millis(200),pausePane);
        fadeIn.setDelay(Duration.millis(50));
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);
        fadeIn.play();
        fadeIn.setOnFinished(event -> {
            pausePane.setOpacity(1.0);
        });

    }
    public void fadeOut() {
        volumeSlider.setDisable(true);
        FadeTransition fadeOut = new FadeTransition(Duration.millis(200),pausePane);
        fadeOut.setDelay(Duration.millis(50));
        fadeOut.setFromValue(1.0);
        fadeOut.setToValue(0.0);
        fadeOut.play();
        fadeOut.setOnFinished(event -> {
            pausePane.setOpacity(0);
            Main.stage.requestFocus();
        });

    }

    public void setOptions(Platform sV) {
        volumeSlider.setMax(1);
        volumeSlider.setMin(0);
        //If the music has no stage, this doesn't do anything
        if (sV.music!=null) {
            volumeSlider.setValue(sV.musicPlayer.volumeProperty().get());
            sV.musicPlayer.volumeProperty().bindBidirectional(volumeSlider.valueProperty());
            sV.musicPlayer.muteProperty().bindBidirectional(mute.selectedProperty());

        }
    }


    public void initialize() {
        //On initialized, this pane shouldn't be yet visible
        pausePane.setOpacity(0);
        randomText.setText(exitText);
        randomText1.setText(backText);
        randomText2.setText(surpiseText);
        volumeSlider.setFocusTraversable(false);
        volumeSlider.setShowTickMarks(true);
        volumeSlider.setBlockIncrement(0.1);
        volumeSlider.setMajorTickUnit(0.1);
        volumeSlider.setSnapToTicks(true);
        volumeSlider.setMinorTickCount(0);
        volumeSlider.setShowTickLabels(false);
    }
}
