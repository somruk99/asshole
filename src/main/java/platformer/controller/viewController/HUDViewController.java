package platformer.controller.viewController;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.io.IOException;
import java.util.ArrayList;

import platformer.model.Character;

public class HUDViewController {


    public Text player1Name;
    public ProgressBar player1HealthBar;

    public int player1MaxHealth;
    public int player1CurrentHealth;
    public Text player2Name;
    public SimpleFloatProperty player1HealthDisplay = new SimpleFloatProperty(0);


    public ProgressBar player2HealthBar;
    public int player2MaxHealth;
    public int player2CurrentHealth;
    public SimpleFloatProperty player2HealthDisplay = new SimpleFloatProperty(0);

    public Text winnerDisplay;

    final Font f = Font.loadFont(this.getClass().getResourceAsStream("/font/agency-fb_[allfont.net].ttf"), 60);

    public void initialize() throws IOException {
        player1Name.setFont(f);
        player2Name.setFont(f);
        winnerDisplay.setFont(f);
        winnerDisplay.setFill(Color.BLANCHEDALMOND);
        winnerDisplay.setVisible(false);
    }

    public void setUpHUD(ArrayList<Character> characterList) {
        player1Name.setText(characterList.get(0).getName());
        player1MaxHealth = characterList.get(0).getStats().getHealth();
        player1CurrentHealth = player1MaxHealth;
        player1HealthBar.progressProperty().bind(player1HealthDisplay);
        rerenderHealth(1,player1CurrentHealth);
        player2Name.setText(characterList.get(1).getName());
        player2MaxHealth = characterList.get(1).getStats().getHealth();
        player2CurrentHealth = player2MaxHealth;
        player2HealthBar.progressProperty().bind(player2HealthDisplay);
        rerenderHealth(2,player2CurrentHealth);
    }

    public Timeline createBarAnimations(SimpleFloatProperty healthProperty, float currentHealth, float maxHealth) {
        return new Timeline(
                new KeyFrame(
                        Duration.seconds(1.2),
                        new KeyValue(healthProperty, Math.abs(currentHealth / maxHealth), Interpolator.EASE_IN)
                )
        );
    }

    public void rerenderHealth(int player,int playerHealth) {
        if (player == 1) {
            player1CurrentHealth = playerHealth;
            Timeline renderHealth = createBarAnimations(player1HealthDisplay, player1CurrentHealth, player1MaxHealth);
            renderHealth.playFromStart();
        }
        if (player == 2) {
            player2CurrentHealth = playerHealth;
            Timeline renderHealth = createBarAnimations(player2HealthDisplay, player2CurrentHealth, player2MaxHealth);
            renderHealth.playFromStart();
        }

    }

    public void declareWinner(ArrayList<Character> characters, int winner) {
        winnerDisplay.setVisible(true);
        winnerDisplay.setText(characters.get(winner).getName() +" Wins!");
        Timeline timeline = new Timeline(new KeyFrame(
                Duration.millis(2500),
                ae -> winnerDisplay.setVisible(false)));
        timeline.play();

    }
    public void reset(ArrayList<Character> characters) {
        for(Character c : characters) {
            c.respawnNewGame();
        }
        setUpHUD(characters);

    }



}
