package platformer.controller.viewController;

import javafx.animation.*;
import javafx.application.Platform;
import javafx.scene.effect.MotionBlur;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;
import platformer.Main;
import platformer.model.gameStage.GameStage;
import platformer.model.gameStage.GameStageCollection;
import platformer.view.TestCharacterSelectScreen;

import java.io.IOException;
import java.util.ArrayList;



public class OverlayMapSelectController {

    public AnchorPane mapSelectRoot;
    public GridPane mapSelectGrid;
    public VBox mapSelectSlot;

    public ImageView selectedMapPreview;
    public Text selectedMapName;

    public Text selectedMapNamePrevious;
    public ImageView selectedMapPreviewPrevious;
    public Text selectedMapNameNext;
    public ImageView selectedMapPreviewNext;

    public GameStage selectedMap;

    private ArrayList<GameStage> stageList = GameStageCollection.stageList;

    public boolean isStageSelected() { return stageSelected; }
    public void setStageSelected(boolean stageSelected) { this.stageSelected = stageSelected; }

    public boolean stageSelected;


    private final int totalStages = stageList.size();
    private int stageIndex = 0;

    final Font f = Font.loadFont(this.getClass().getResourceAsStream("/font/agency-fb_[allfont.net].ttf"),60);
    private String characterTextStyle = "-fx-stroke: white;\n" + "    -fx-stroke-width: 2;";

    public GameStage getSelectedMap() {
        return selectedMap;
    }

    public void selectStage() {

        if (!stageSelected) {
            Platform.runLater(() -> {
                stageSelected = true;
            });
            Animation animation = new Timeline(
                    new KeyFrame(Duration.seconds(1.5),
                            new KeyValue(selectedMapPreview.fitHeightProperty(), 720),
                            new KeyValue(selectedMapPreview.fitWidthProperty(), 1280)));
            animation.setDelay(Duration.millis(1000));
            animation.play();
            animation.setOnFinished(event -> {
                //start loading
                try {
                    Platform.runLater(() -> {
                        Main.startPlatformGame(selectedMap, TestCharacterSelectScreen.selectedCharacterOne,TestCharacterSelectScreen.selectedCharacterTwo);
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

        }
    }


    public void processKey(KeyCode key) {
        if (stageSelected) return;
        {
            if (key.equals(platformer.model.KeySettings.p1.up)) {
                stageIndex++;
            }
            if (key.equals(platformer.model.KeySettings.p1.down)) {
                stageIndex--;
            }
            changeStage();
        }
        {
            //other crap
            if (key.equals(platformer.model.KeySettings.p1.enter)) {
                selectStage();
            }

        }

    }


    public void initialize() throws IOException {
        stageSelected = false;
        changeStage();
        mapSelectRoot.setOpacity(0);
    }
    public void setVisible() {
        if (mapSelectRoot.getOpacity() == 0 ) {
            mapSelectRoot.setEffect(new MotionBlur());
            FadeTransition fadeIn = new FadeTransition(Duration.millis(300), mapSelectRoot);
            fadeIn.setDelay(Duration.millis(50));
            fadeIn.setFromValue(0.0);
            fadeIn.setToValue(1.0);

            fadeIn.play();
            fadeIn.setOnFinished(event -> {
                mapSelectRoot.setOpacity(1.0);
                mapSelectRoot.setEffect(null);
            });
        }
    }

    private void changeStage() {
        //Calculate current Index
        {
            if (stageIndex == totalStages) {
                stageIndex = 0;
            }
            if (stageIndex == -1) {
                stageIndex = totalStages - 1;
            }
            selectedMap = stageList.get(stageIndex);
            selectedMapPreview.setImage(selectedMap.getStageImage());
            selectedMapName.setText(selectedMap.getStageName());
        }
        //Calculate previous Index
        {
            int previousIndex = stageIndex == 0? totalStages-1 :  stageIndex-1;
            selectedMapNamePrevious.setText(
                    stageList.get(previousIndex).getStageName()
            );
            selectedMapPreviewPrevious.setImage(
                    stageList.get(previousIndex).getStageImage()
            );
        }
        //Calculate next Index
        {
            int nextIndex =  stageIndex == totalStages-1? 0 : stageIndex+1;
            selectedMapNameNext.setText(
                    stageList.get(nextIndex).getStageName()
            );
            selectedMapPreviewNext.setImage(
                    stageList.get(nextIndex).getStageImage()
            );
        }

    }
}
