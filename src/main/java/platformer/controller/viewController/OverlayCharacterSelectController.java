package platformer.controller.viewController;


import javafx.animation.*;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;
import platformer.view.StatusView;
import platformer.view.TestCharacterSelectScreen;
import platformer.model.Character;
import platformer.model.Keys;
import platformer.view.TestCharacterSelectScreen;
import platformer.view.CharacterSlots;

import java.io.IOException;
import java.util.Arrays;

public class OverlayCharacterSelectController {
    public StackPane stackPane;


    //For player1
    public GridPane playerOneSelect;
    public GridPane p1CharacterSelectSlots;
    public ImageView p1CharacterPortrait;
    public Text p1SelectedCharacterName;
    public HBox p1StatusViewBox;
    public StatusView p1StatusView;
    private Character SelectedPlayerOneCharacter;
    private KeyCode p1Upkey;
    private KeyCode p1DownKey;
    private KeyCode p1LeftKey;
    private KeyCode p1RightKey;
    private KeyCode p1SelectKey;
    private KeyCode p1UnselectKey;
    private static boolean p1Selected = false;

    //For player2
    public GridPane playerTwoSelect;
    public GridPane p2CharacterSelectSlots;
    public ImageView p2CharacterPortrait;
    public Text p2SelectedCharacterName;
    public HBox p2StatusViewBox;
    public StatusView p2StatusView;
    private Character SelectedPlayerTwoCharacter;
    private KeyCode p2Upkey;
    private KeyCode p2DownKey;
    private KeyCode p2LeftKey;
    private KeyCode p2RightKey;
    private KeyCode p2SelectKey;
    private KeyCode p2UnselectKey;
    private static boolean p2Selected = false;


    public static CharacterSlots p1CharacterSlots;
    public static CharacterSlots p2CharacterSlots;
    public  boolean allSelected = false;
    final Font f = Font.loadFont(this.getClass().getResourceAsStream("/font/agency-fb_[allfont.net].ttf"),60);

    private String characterTextStyle = "-fx-stroke: white;\n" + "    -fx-stroke-width: 2;";


    public Keys getKeyPress() { return keyPress; }

    public void setKeyPress(Keys keyPress) { this.keyPress = keyPress; }

    public Keys keyPress = new Keys();

    public void setKeyP1(KeyCode up, KeyCode down, KeyCode left, KeyCode right, KeyCode select, KeyCode unselect) {
        p1Upkey = up;
        p1DownKey = down;
        p1LeftKey = left;
        p1RightKey = right;
        p1SelectKey = select;
        p1UnselectKey = unselect;
        p1CharacterSlots.setUpKey(up);
        p1CharacterSlots.setDownKey(down);
        p1CharacterSlots.setLeftKey(left);
        p1CharacterSlots.setRightKey(right);
    }
    public void setKeyP2(KeyCode up, KeyCode down, KeyCode left, KeyCode right,KeyCode select, KeyCode unselect) {
        p2Upkey = up;
        p2DownKey = down;
        p2LeftKey = left;
        p2RightKey = right;
        p2SelectKey = select;
        p2UnselectKey = unselect;
        p2CharacterSlots.setUpKey(up);
        p2CharacterSlots.setDownKey(down);
        p2CharacterSlots.setLeftKey(left);
        p2CharacterSlots.setRightKey(right);
    }
    private void renderSelectedCharacter(Character playerChar,Text playerCharName,StatusView statusView,ImageView playerCharPortrait) {

        TranslateTransition move = new TranslateTransition(Duration.millis(200));

        FadeTransition fade = new FadeTransition(Duration.millis(200));
        {
            move.setFromX(0);
            move.setToX(-100);

            fade.setFromValue(1.0);
            fade.setToValue(0);

            playerCharName.setText(playerChar.getName());
            statusView.rerenderStats(playerChar.getStats());

            ParallelTransition pt = new ParallelTransition(playerCharPortrait, move, fade);

            pt.setOnFinished(event -> {

                move.setFromX(-100);
                move.setToX(0);

                fade.setFromValue(0);
                fade.setToValue(1.0);

                ParallelTransition pt2 = new ParallelTransition(playerCharPortrait, move, fade);
                playerCharPortrait.setImage(playerChar.getCharacterPortrait());
                pt2.setDelay(Duration.millis(40));
                pt2.play();
            });
            pt.play();
        }
    }

    private void pushBackSelect(Pane pane,int player) {
        TranslateTransition move2 = new TranslateTransition(Duration.millis(200),pane);
        move2.setFromX(0);
        move2.setToX(player==1? -200:200);
        move2.play();
    }
    private void pushForwardSelect(Pane pane,int player) {
        TranslateTransition move2 = new TranslateTransition(Duration.millis(200),pane);
        move2.setFromX(player==1? -200:200);
        move2.setToX(0);
        move2.play();
    }

    public void timeupSelect() {
        selectCharacter(true,1);
        selectCharacter(true,2);


    }

    public void selectCharacter(boolean select, int player) {

        if (select) {
            if (player == 1) {
                p1CharacterSlots.setSelected();
                p1Selected = true;
                pushBackSelect(playerOneSelect,1);
            }
            else if (player == 2) {
                p2CharacterSlots.setSelected();
                p2Selected = true;
                pushBackSelect(playerTwoSelect,2);
            }
        }
        else  {
            if (player == 1) {
                p1CharacterSlots.setUnselected();
                p1Selected = false;
                pushForwardSelect(playerOneSelect,1);
            }
            else if (player == 2) {
                p2CharacterSlots.setUnselected();
                p2Selected = false;
                pushForwardSelect(playerTwoSelect,2);
            }
        }
        //If incase both are the same
        if(p1SelectedCharacterName.getText().equalsIgnoreCase(p2SelectedCharacterName.getText())&& (p1Selected && p2Selected)) {
            p1Selected=false;
            p2Selected=false;
            if(Math.random() >= 0.5) {

                if(Math.random() >= 0.5) {
                    processKey(p1Upkey);
                }
                else{
                    processKey(p1DownKey);
                }
            }
            else
            {
                if (Math.random()>=0.5) {
                    processKey(p2Upkey);
                }
                else {
                    processKey(p2DownKey);
                }
            }
            p1Selected=true;
            p2Selected=true;
        }

        if (p1Selected && p2Selected) allSelected=true;

    }


    public void processKey(KeyCode key) {
        KeyCode[] tempCodep1 =  {p1Upkey,p1DownKey,p1LeftKey,p1RightKey,p1SelectKey,p1UnselectKey};
        KeyCode[] tempCodep2 =  {p2Upkey,p2DownKey,p2LeftKey,p2RightKey,p2SelectKey,p2UnselectKey};
        if (Arrays.asList(tempCodep1).contains(key)) {
            //Press select key
            if (key.equals(p1SelectKey)) {
                if (!p1Selected) {
                    if ((p2Selected) && p2CharacterSlots.currentSelected.getCharacter().getName().equalsIgnoreCase(p1CharacterSlots.currentSelected.getCharacter().getName())) return;
                    selectCharacter(true,1);
                }
            }
            //Press unselect key
            if (key.equals(p1UnselectKey)) {
                if (p1Selected) {
                    selectCharacter(false,1);
                }
            }

            if (!p1Selected) {
                p1CharacterSlots.processKeyPress(key);
            }
        }
        if (Arrays.asList(tempCodep2).contains(key)) {
            //Press select key
            if (key.equals(p2SelectKey)) {
                if (!p2Selected) {
                    if ((p1Selected) && p2CharacterSlots.currentSelected.getCharacter().getName().equalsIgnoreCase(p1CharacterSlots.currentSelected.getCharacter().getName())) return;
                    selectCharacter(true,2);
                }
            }
            //Press unselect key
            if (key.equals(p2UnselectKey)) {
                if (p2Selected) {
                    selectCharacter(false,2);
                }
            }
            if (!p2Selected) {
                p2CharacterSlots.processKeyPress(key);
            }
        }



    }


    public void initialize() throws IOException {
        p1Selected = false;
        p2Selected = false;
        allSelected = false;
        p1SelectedCharacterName.setFont(f);
        p2SelectedCharacterName.setFont(f);
        p1SelectedCharacterName.setStyle(characterTextStyle);
        p2SelectedCharacterName.setStyle(characterTextStyle);
        p1CharacterPortrait.toBack();
        p2CharacterPortrait.toBack();
        p1StatusView = new StatusView(TestCharacterSelectScreen.selectedCharacterOne.getStats());
        p1CharacterPortrait.setImage(TestCharacterSelectScreen.selectedCharacterOne.getCharacterPortrait());
        p2StatusView = new StatusView(TestCharacterSelectScreen.selectedCharacterTwo.getStats());
        p2CharacterPortrait.setImage(TestCharacterSelectScreen.selectedCharacterTwo.getCharacterPortrait());

        p1StatusViewBox.getChildren().add(p1StatusView);

        p2StatusViewBox.getChildren().add(p2StatusView);
        p1CharacterSlots = new CharacterSlots(1);
        p1CharacterSelectSlots.add(p1CharacterSlots,0,0,2,3);
        p2CharacterSlots = new CharacterSlots(2);

        p2CharacterSelectSlots.add(p2CharacterSlots,0,0,2,3);

        p1SelectedCharacterName.setText(TestCharacterSelectScreen.selectedCharacterOne.getName());
        p2SelectedCharacterName.setText(TestCharacterSelectScreen.selectedCharacterTwo.getName());


        Task<Void> renderSelectedCharacter = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                while (!this.isCancelled()) {

                    if(!p1SelectedCharacterName.getText().equals(TestCharacterSelectScreen.selectedCharacterOne.getName())) {
                        Platform.runLater(() -> {
                            renderSelectedCharacter(TestCharacterSelectScreen.selectedCharacterOne,p1SelectedCharacterName,p1StatusView,p1CharacterPortrait);
                        });
                    }
                    if(!p2SelectedCharacterName.getText().equals(TestCharacterSelectScreen.selectedCharacterTwo.getName())) {
                        Platform.runLater( () -> {
                            renderSelectedCharacter(TestCharacterSelectScreen.selectedCharacterTwo,p2SelectedCharacterName,p2StatusView,p2CharacterPortrait);
                        });

                    }
                    Thread.sleep(25);
                    //If both characters have been selected
                    if (allSelected){//p1Selected && p2Selected) {
                        Thread.sleep(250);
                        allSelected  = true;

                        TestCharacterSelectScreen.overlayMapSelectController.setVisible();
                        Timeline timeline = new Timeline(new KeyFrame(
                                Duration.millis(2000),
                                ae -> this.cancel()));
                        timeline.play();
                    }

                }
                return null;
            }
        };

        new Thread(renderSelectedCharacter).start();










    }
}

