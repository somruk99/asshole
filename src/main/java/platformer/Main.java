package platformer;


import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import platformer.controller.DrawingLoop;
import platformer.controller.GameLoop;
import platformer.model.gameStage.GameStage;
import platformer.view.Platform;
import platformer.view.SelectScreen;
import platformer.model.Character;
import platformer.view.TestCharacterSelectScreen;

import java.io.IOException;

public class Main extends Application {

    public static boolean isPaused() { return pause; }
    public static void setPause(boolean pause) { Main.pause = pause; }

    public static boolean pause;
    public static GameLoop gameLoop;
    public static DrawingLoop drawingLoop;
    public static Platform platform;
    public static TestCharacterSelectScreen selectScreen;
    public static Stage stage;

    public static void main(String[] args) { launch(args); }

    @Override
    public void start(Stage primaryStage) throws IOException, InterruptedException {

        stage = primaryStage;
        Scene scene = new Scene(selectScreen = new TestCharacterSelectScreen(),platform.WIDTH,platform.HEIGHT);
        scene.setOnKeyPressed(event-> {
            selectScreen.processKey(event.getCode());
        });

        primaryStage.setTitle("platformer");
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static void startPlatformGame(GameStage gameStage,Character charOne,Character charTwo) {
        selectScreen.fadeOutMusic();
        platform = new Platform(gameStage,charOne,charTwo);
        gameLoop = new GameLoop(platform);
        drawingLoop = new DrawingLoop(platform);

        Scene scene = new Scene(platform,platform.WIDTH,platform.HEIGHT);
        scene.setOnKeyPressed(event-> {
            KeyCode key = event.getCode();
            if (key == KeyCode.ESCAPE) {
                if (gameLoop.pause.isPause()) Main.shutDown();
            } else if (key == KeyCode.P) {
                gameLoop.pause.setPause(!gameLoop.pause.isPause());
            } else {
                platform.getKeys().add(key);
            }
        });
        scene.setOnKeyReleased(event ->  platform.getKeys().remove(event.getCode()));
        stage.setScene(scene);

        (new Thread(gameLoop)).start();
        (new Thread(drawingLoop)).start();
    }

    public static void shutDown() {
        javafx.application.Platform.exit();
        System.exit(0);
    }
}



