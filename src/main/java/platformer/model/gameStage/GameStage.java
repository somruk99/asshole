package platformer.model.gameStage;

import javafx.scene.image.Image;

public class GameStage {

    private int stageGround;
    private Image stageImage;
    private String stageName;
    private String music;




    public GameStage(int stageGround, Image stageImg,String stageName,String musicFile) {
        this.stageGround = stageGround;
        stageImage = stageImg;
        this.stageName = stageName;
        this.music = musicFile;
    }

    public int getStageGround() { return stageGround; }
    public Image getStageImage() { return stageImage; }
    public String getStageName() { return stageName; }
    public String getMusic() { return music;}

    public GameStage(int stageGround, Image stageImg,String stageName) {
        this.stageGround = stageGround;
        stageImage = stageImg;
        this.stageName = stageName;
        this.music = null;
    }






}
