package platformer.model.gameStage;

import javafx.scene.image.Image;


import java.util.ArrayList;

public class GameStageCollection {

    public static final ArrayList<GameStage> stageList = new ArrayList<>();
    static {
        stageList.add(new GameStage(
                540,
                new Image(GameStageCollection.class.getResourceAsStream("/stages/default/Background.png")),
                "Lab 7 & Lab 8"
        ));
        stageList.add(new GameStage(
                600,
                new Image(GameStageCollection.class.getResourceAsStream("/stages/e1m1/e1m1.gif")),
                "E1M1",
                "e1m1 theme song.mp3"
        ));
        stageList.add(new GameStage(
                650,
                new Image(GameStageCollection.class.getResourceAsStream("/stages/backtochina/testStage.png")),
                "Back to China",
                "Back to the China theme song.mp3"
        ));
    }
}

