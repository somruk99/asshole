package platformer.model;

import javafx.scene.input.KeyCode;

public class KeySettings {

    public static KeyCode pause = KeyCode.P;

    public static class p1 {
        public static KeyCode up = KeyCode.W;
        public static KeyCode down = KeyCode.S;
        public static KeyCode left = KeyCode.A;
        public static KeyCode right = KeyCode.D;
        public static KeyCode enter = KeyCode.SPACE;
        public static KeyCode cancel  = KeyCode.SHIFT;
    }
    public static class p2 {
        public static KeyCode up = KeyCode.L;//KeyCode.OPEN_BRACKET;
        public static KeyCode down = KeyCode.PERIOD;//KeyCode.QUOTE;

        public static KeyCode left = KeyCode.COMMA;//KeyCode.SEMICOLON;
        public static KeyCode right = KeyCode.SLASH;//KeyCode.BACK_SLASH;
        public static KeyCode enter = KeyCode.ENTER;
        public static KeyCode cancel = KeyCode.BACK_SPACE;
    }
}
