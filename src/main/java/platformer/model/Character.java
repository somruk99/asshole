package platformer.model;

import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import platformer.view.Platform;

import java.util.concurrent.TimeUnit;

public class Character extends CharacterBase {

    Logger logger = LoggerFactory.getLogger(Character.class);

    public static final int CHARACTER_WIDTH = (int)(32*1.6);
    public static final int CHARACTER_HEIGHT = (int)(64*1.8);

    private Image characterImg;
    private int x;
    private int y;
    private int startX;
    private int startY;
    private int offsetX;
    private int offsetY;


    private AnimatedSprite imageView;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }


    public int getOffsetX() {
        return this.offsetX;
    }

    public int getOffsetY() {
        return this.offsetY;
    }


    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isMoveLeft() {
        return isMoveLeft;
    }

    public boolean isMoveRight() {
        return isMoveRight;
    }

    public boolean isCanJump() {
        return canJump;
    }

    boolean movementLocked = false;
    public void setMovementLocked(boolean movementLocked) { this.movementLocked = movementLocked; }



    private KeyCode leftKey;
    private KeyCode rightKey;
    private KeyCode upKey;

    int xVelocity = 0;
    int yVelocity = 0;
    int xAcceleration = 1;
    int yAcceleration = 1;


    boolean isMoveLeft = false;
    boolean isMoveRight = false;
    boolean falling = true;
    boolean canJump = false;
    boolean jumping = false;

    private int score=0;
    public int getScore() { return score; }

    public void setStartX(int startX) { this.startX = startX; }

    public Character(String name,String characterPortraitFile,String characterSlotFile, CharacterStats stats ,Image characterImg,AnimatedSprite sprite) {
        super(name, characterPortraitFile,characterSlotFile,stats);

        this.characterImg = characterImg;
        this.imageView = sprite;
        this.imageView.setFitWidth(CHARACTER_WIDTH);
        this.imageView.setFitHeight(CHARACTER_HEIGHT);
        this.getChildren().addAll(this.imageView);
    }

    public void respawnNewGame() {
        this.setVisible(true);
        stats.refillHealth();
        respawn();
    }

    public void respawn() {
        x = startX;
        y = startY;
        imageView.setFitWidth(CHARACTER_WIDTH);
        imageView.setFitHeight(CHARACTER_HEIGHT);
        isMoveLeft = false;
        isMoveRight = false;
        falling = true;
        canJump = false;
        jumping = false;
    }

    public void moveLeft() {
        isMoveLeft = true;
        isMoveRight = false;
    }
    public void moveRight() {
        isMoveRight = true;
        isMoveLeft = false;
    }

    public void stop() {
        isMoveLeft = false;
        isMoveRight = false;
        xVelocity = 0;
    }

    public void jump() {
        if (canJump) {
            yVelocity = yMaxVelocity;
            canJump = false;
            jumping = true;
            falling = false;
        }
    }

    public void checkReachHighest() {
        if(jumping &&  yVelocity <= 0) {
            jumping = false;
            falling = true;
            yVelocity = 0;
        }
    }

    public void checkReachFloor() {
        if(falling && y >= Platform.GROUND - CHARACTER_HEIGHT) {
 		    y = Platform.GROUND - CHARACTER_HEIGHT;
            falling = false;
            canJump = true;
            yVelocity = 0;
        }
    }

    public void checkReachGameWall() {
        if(x <= 0) {
            x = 0;
        } else if( x+getWidth() >= Platform.WIDTH) {
            x = Platform.WIDTH-CHARACTER_WIDTH;
        }
    }

    public void collided(Character c) throws InterruptedException {
        if (isMoveLeft) {
            x = c.getX() + CHARACTER_WIDTH + 1;
            stop();
        } else if (isMoveRight) {
            x = c.getX() - CHARACTER_WIDTH - 1;
            stop();
        }

        //Not on the ground
        if(y < Platform.GROUND - CHARACTER_HEIGHT) {
            //Above c
            if( falling && y < c.getY() && Math.abs(y-c.getY())<=CHARACTER_HEIGHT+1) {
                score++;
                y = Platform.GROUND - CHARACTER_HEIGHT - 5;
                repaint();
                c.collapsed();
                c.getStats().setHealth(c.getStats().getHealth()-calculateAttackPoint());
                if (c.getStats().getHealth()<=0) {
                    c.getStats().setHealth(0);
                    c.collapsed();
                    score+=4;
                }
                else {
                    c.collapsed();
                    c.setMovementLocked(true);
                    c.respawn();
                    c.setMovementLocked(false);
                }
            }
        }
    }

    public void collapsed() throws InterruptedException {
        imageView.setFitHeight(5);
        y = Platform.GROUND - 5;
        repaint();
        TimeUnit.MILLISECONDS.sleep(500);
    }



    public void moveX() {
        setTranslateX(x);

        if(isMoveLeft) {
            xVelocity = xVelocity >= xMaxVelocity? xMaxVelocity : xVelocity+xAcceleration;
            x = x - xVelocity;
        }
        if(isMoveRight) {
            xVelocity = xVelocity >= xMaxVelocity? xMaxVelocity : xVelocity+xAcceleration;
            x = x + xVelocity;
        }
    }

    public void moveY() {
        setTranslateY(y);

        if(falling) {
            yVelocity = yVelocity >= yMaxVelocity? yMaxVelocity : yVelocity+yAcceleration;
            y = y + yVelocity;
        }
        else if(jumping) {
            yVelocity = yVelocity <= 0 ? 0 : yVelocity-yAcceleration;
            y = y - yVelocity;
        }
    }

    public void repaint() {
        moveX();
        moveY();
    }

    public void trace() {
        logger.debug("x:{} y:{} vx:{} vy:{}",x,y,xVelocity,yVelocity);
    }

    public void setLeftKey(KeyCode leftKey) {
        this.leftKey = leftKey;
    }

    public void setRightKey(KeyCode rightKey) {
        this.rightKey = rightKey;
    }

    public void setUpKey(KeyCode upKey) {
        this.upKey = upKey;
    }

    public KeyCode getLeftKey() {
        return leftKey;
    }

    public KeyCode getRightKey() {
        return rightKey;
    }

    public KeyCode getUpKey() {
        return upKey;
    }

    public AnimatedSprite getImageView() { return imageView; }
}
