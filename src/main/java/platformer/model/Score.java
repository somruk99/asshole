package platformer.model;

import javafx.scene.layout.Pane;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class Score extends Pane {
    Label point;
    final Font f = Font.loadFont(this.getClass().getResourceAsStream("/font/agency-fb_[allfont.net].ttf"),60);
    public Score(int x, int y) {
        point = new Label("0");
        setTranslateX(x);
        setTranslateY(y);
        point.setFont(f);
        point.setTextFill(Color.web("#FFF"));
        getChildren().addAll(point);
    }
    public void setPoint(int score) {
        this.point.setText(Integer.toString(score));
    }
}
