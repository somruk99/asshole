package platformer.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

public class Pause {

    private BooleanProperty pause = new SimpleBooleanProperty();
    public final boolean isPause() { return pause.get(); }
    public final void setPause(boolean pause) { this.pause.set(pause);}
    public BooleanProperty pauseProperty() { return pause;}

}
