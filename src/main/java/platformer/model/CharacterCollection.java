package platformer.model;

import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CharacterCollection {


    public static Map<String,Character> allCharacters = new HashMap<>();
    public static Map<String,Character> allCharactersNoRandom;
    private static String[] allCharactersNoRandomNameKeys;


    static {
        /*Please put all the necessary Character Classes, image files and AnimatedSprite Classes here*/
        mario : {
            Image marioImage = new Image(CharacterCollection.class.getResourceAsStream("/characters/mario/MarioSheet.png"));
            AnimatedSprite marioSpriteSheet = new AnimatedSprite(marioImage, 4, 4, 0, 0, 16, 32);
            String marioPortraitFile = "/characters/mario/8-bit-mario-cannon-png-2.png";
            String marioSlotFile = "/characters/mario/8-bit-mario-cannon-png-2.png";
            Character mario = new Character("Mario", marioPortraitFile, marioSlotFile,new CharacterStats(), marioImage, marioSpriteSheet);
            allCharacters.put(mario.getName(), mario);
        }
        batman : {
            Image batmanImage = new Image(CharacterCollection.class.getResourceAsStream("/characters/batman/testBatman.png"));
            AnimatedSprite batmanSpriteSheet = new AnimatedSprite(batmanImage,16,16,0,0,39,72);
            String batmanPortraitFile = "/characters/batman/Batman.png";
            String batmanSlotFile = "/characters/batman/Batman.png";
            CharacterStats batmanStats = new CharacterStats(75,65,80,60);
            Character batman = new Character("Batman", batmanPortraitFile,batmanSlotFile,batmanStats,batmanImage,batmanSpriteSheet);
            allCharacters.put(batman.getName(), batman);
        }
        doomguy: {
            Image doomguyImage = new Image(CharacterCollection.class.getResourceAsStream("/characters/doomguy/doomSprite.png"));
            AnimatedSprite doomguySpriteSheet = new AnimatedSprite(doomguyImage,4,4,0,0,42,55);
            doomguySpriteSheet.flipSprite();
            String doomguyPortraitFile = "/characters/doomguy/doomFace.png";
            String doomguySlotFile = "/characters/doomguy/doomFace.png";
            CharacterStats doomguyStats = new CharacterStats(90,95,50,40);
            Character doomguy = new Character("Doom Guy", doomguyPortraitFile,doomguySlotFile,doomguyStats,doomguyImage,doomguySpriteSheet);
            allCharacters.put(doomguy.getName(), doomguy);
        }
        //Random Character, don't actually use except for random select
        random: {
            Image rndImage = new Image(CharacterCollection.class.getResourceAsStream("/characters/random.png"));
            AnimatedSprite doomguySpriteSheet = new AnimatedSprite(rndImage,4,4,0,0,42,55);
            doomguySpriteSheet.flipSprite();
            String doomguyPortraitFile = "/characters/random.png";
            String doomguySlotFile = "/characters/random.png";
            CharacterStats doomguyStats = new CharacterStats(999,999,999,999);
            Character doomguy = new Character("Random", doomguyPortraitFile,doomguySlotFile,doomguyStats,rndImage,doomguySpriteSheet);
            allCharacters.put(doomguy.getName(), doomguy);

        }
        allCharactersNoRandom = new HashMap<>(allCharacters);
        allCharactersNoRandom.remove("Random");
        allCharactersNoRandomNameKeys = allCharactersNoRandom.keySet().toArray(new String[allCharactersNoRandom.size()]);

    }

    public static void resolveRandomCharcters(ArrayList<Character> charList) {
        String[] nameKeys = allCharactersNoRandomNameKeys;
        List<String> charListNameKey = charList.stream().map(c -> c.getName()).collect(Collectors.toList());
        String key;
        for (int i=0; i< charList.size();i++) {
            if (charList.get(i).getName().equalsIgnoreCase("Random")) {
                do {
                    key = nameKeys[(int) (Math.random() * nameKeys.length)];
                } while (charListNameKey.contains(key));
                charList.set(i,CharacterCollection.allCharactersNoRandom.get(key));
                charListNameKey.set(i,key);
            }
        }
    }

}

