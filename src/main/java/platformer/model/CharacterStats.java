package platformer.model;

public class CharacterStats {

    private Integer health;

    public Integer getMaxHealth() {
        return maxHealth;
    }

    private Integer maxHealth;
    private Integer attack;
    private Integer agility;
    private Integer speed;

    //Default stats
    public CharacterStats() {
        this(50,50,50,50);
    }

    public void refillHealth() {
        health = maxHealth;
    }

    public CharacterStats(Integer health, Integer attack, Integer agility, Integer speed) {
        this.health = health;
        this.maxHealth = health;
        this.attack = attack;
        this.agility = agility;
        this.speed = speed;
    }

    public Integer getHealth() { return health; }
    public void setHealth(Integer health) { this.health = health; }

    public Integer getAttack() { return attack; }
    public void setAttack(Integer attack) { this.attack = attack; }

    public Integer getAgility() { return agility; }
    public void setAgility(Integer agility) { this.agility = agility; }

    public Integer getSpeed() { return speed; }
    public void setSpeed(Integer speed) { this.speed = speed; }

}
