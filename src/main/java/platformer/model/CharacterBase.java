package platformer.model;

import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

public abstract class CharacterBase extends Pane {


    //Fields
    protected String name;

    public Image getCharacterPortrait() {
        return characterPortrait;
    }

    public void setCharacterPortrait(Image characterPortrait) {
        this.characterPortrait = characterPortrait;
    }

    public Image getCharacterSelectSlot() {
        return characterSelectSlot;
    }

    public void setCharacterSelectSlot(Image characterSelectSlot) {
        this.characterSelectSlot = characterSelectSlot;
    }

    protected Image characterPortrait;
    protected Image characterSelectSlot;



    protected CharacterStats stats;
    //Max velocity for moving in x-axis
    protected int xMaxVelocity;
    //Max velocity for moving in y-axis
    protected int yMaxVelocity;

    //Getters and Setters

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public CharacterStats getStats() { return stats; }
    public void setStats(CharacterStats stats) { this.stats = stats; }


    //Constructors
    public CharacterBase() {
        this.xMaxVelocity = 7;
        this.yMaxVelocity = 17;
    }

    public CharacterBase(String name,String characterPortraitFile,String characterSlotFile, CharacterStats stats) {
        this.name = name;
        this.characterPortrait = new Image(getClass().getResourceAsStream(characterPortraitFile));
        this.characterSelectSlot = new Image(getClass().getResourceAsStream(characterSlotFile));
        this.stats = stats;
        calculateMaxVelocity(stats.getSpeed(),stats.getAgility());

    }

    //Methods
    private void calculateMaxVelocity(int speed,int agility) {
        this.xMaxVelocity = (int)Math.round(Math.sqrt(speed/2) +2.4);
        this.yMaxVelocity = (int)Math.round(Math.sqrt(agility/2)+12.4);
    }

    protected int calculateAttackPoint() {
        return (this.stats.getAttack() / 6);
    }


}
