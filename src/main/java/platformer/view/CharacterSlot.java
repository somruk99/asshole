package platformer.view;

import javafx.scene.CacheHint;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import platformer.model.Character;

public class CharacterSlot extends Pane {

    private String unselectedStyle = "-fx-border-color: black;  -fx-border-width: 2.5;";
    private String selectedStyle = "-fx-border-color: yellow;  -fx-border-width: 2.5;";

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }

    private Character character;
    private StackPane slotPane = new StackPane();
    private ImageView slotBg;
    private ImageView slotPortrait;

    public void setSelected() {
        slotPane.setStyle(selectedStyle);

    }

    public void setUnselected() {
        slotPane.setStyle(unselectedStyle);
    }

    public CharacterSlot(Character character) {
        this.character = character;
        slotPane.setPrefSize(112,112);
        slotPane.setStyle(unselectedStyle);
        slotPane.setLayoutX(40);
        slotPane.setLayoutY(24);
        slotPortrait = new ImageView(character.getCharacterSelectSlot());
        slotPortrait.setFitHeight(100);
        slotPortrait.setFitWidth(100);
        slotPortrait.setCache(true);
        slotPortrait.setCacheHint(CacheHint.QUALITY);
        slotBg = new ImageView(new Image(this.getClass().getResourceAsStream("/characters/p1SlotsBg.png")));
        slotBg.setFitHeight(100);
        slotBg.setFitWidth(100);
        slotPane.getChildren().addAll(slotBg,slotPortrait);
        this.getChildren().add(slotPane);

    }

}
