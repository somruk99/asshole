package platformer.view;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import platformer.model.Character;
import platformer.model.CharacterCollection;
import platformer.model.Keys;
import platformer.model.Score;
import platformer.model.gameStage.GameStage;
import platformer.model.gameStage.GameStageCollection;

import java.util.ArrayList;
import java.util.Random;

public class Platform extends ViewBase {

    public static final int WIDTH = 1280;
    public static final int HEIGHT = 720;
    public static int GROUND;

    public static int p1xSpawn;
    public static int p2xSpawn;

    private ImageView backgroundImg;
    private ArrayList<Character> characterList;
    private ArrayList<Score> scoreList;

    private Keys keys;

    public Platform(GameStage stage,Character charOne,Character charTwo) {

        //Booting platform
        {
            characterList = new ArrayList<>();
            scoreList = new ArrayList();
            keys = new Keys();
        }
        //Loading stage part
        {
            GameStage platformStage = stage;
            GROUND = platformStage.getStageGround();
            backgroundImg  = new ImageView(platformStage.getStageImage());
            backgroundImg.setFitHeight(HEIGHT);
            backgroundImg.setFitWidth(WIDTH);

        }
        //Loading characters
        {
            characterList.add(charOne);
            characterList.add(charTwo);
            CharacterCollection.resolveRandomCharcters(characterList);
            p1xSpawn = 30;
            p2xSpawn = WIDTH-p1xSpawn-Character.CHARACTER_WIDTH;
            setSpawnPoint();
        }
        //Set key bindings
        {

            characterList.get(0).setUpKey(platformer.model.KeySettings.p1.up);
            characterList.get(0).setRightKey(platformer.model.KeySettings.p1.right);
            characterList.get(0).setLeftKey(platformer.model.KeySettings.p1.left);
            characterList.get(1).setUpKey(platformer.model.KeySettings.p2.up);
            characterList.get(1).setRightKey(platformer.model.KeySettings.p2.right);
            characterList.get(1).setLeftKey(platformer.model.KeySettings.p2.left);

        }
        //Loading score
        {
            scoreList.add(new Score(50, HEIGHT - 140));
            scoreList.add(new Score(WIDTH - 50, HEIGHT - 140));
        }
        //Baking everything together
        {
            getChildren().add(backgroundImg);
            getChildren().addAll(characterList);
            getChildren().addAll(scoreList);
        }

        //Set the stage's theme song
        musicFile = stage.getMusic();
        setUpMusic(0,0,0,true);

    }


    public Platform() {
        this(GameStageCollection.stageList.get(new Random().nextInt(GameStageCollection.stageList.size())),
                CharacterCollection.allCharacters.get("Mario"),
                CharacterCollection.allCharacters.get("Batman"));
    }

    private void setSpawnPoint() {
        //Spawn points if respawn
        this.characterList.get(0).setStartX(p1xSpawn);
        this.characterList.get(1).setStartX(p2xSpawn);
        this.characterList.get(0).setX(p1xSpawn);
        this.characterList.get(1).setX(p2xSpawn);
        this.characterList.get(0).setY(this.GROUND+Character.CHARACTER_HEIGHT);
        this.characterList.get(1).setY(this.GROUND+Character.CHARACTER_HEIGHT);
    }

    public ArrayList<Character> getCharacterList() { return characterList; }
    public ArrayList<Score> getScoreList() { return scoreList; }
    public Keys getKeys() { return keys; }
}

