package platformer.view;

import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;
import platformer.controller.viewController.OverlayCharacterSelectController;

import platformer.controller.viewController.OverlayMapSelectController;
import platformer.model.Character;
import platformer.model.CharacterCollection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TestCharacterSelectScreen extends ViewBase {



    private final StackPane selectStackPane;
    private final MediaView videoBackground;
    private final BorderPane selectView;


    //Time count down
    private static Integer STARTTIME = 20;
    private Timeline timeline;
    private Label timerLabel = new Label();
    private IntegerProperty timeSeconds = new SimpleIntegerProperty(STARTTIME);


    private String announcerFolder = "/sfx/PlayStation - Tekken 3 - Announcer/Announcer/";


    public static Character selectedCharacterOne;
    public static Character selectedCharacterTwo;
    public  ArrayList<Character> playerCharacters = new ArrayList<>(2);


    private FXMLLoader loader;

    private Map<String,Media> announcerCountDown;
    public OverlayCharacterSelectController overlayController;
    public static OverlayMapSelectController overlayMapSelectController;


    private void setUpPlayerView() throws IOException, InterruptedException {

        new Thread(new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                loader = new FXMLLoader(getClass().getResource("/OverlayCharacterSelect.fxml"));
                Pane overlayCharacterSelectScreen = loader.load();
                overlayController = loader.getController();
                selectStackPane.getChildren().add(overlayCharacterSelectScreen);
                return null;
            }
        }).run();


        overlayController.setKeyP1(
                platformer.model.KeySettings.p1.up,
                platformer.model.KeySettings.p1.down,
                platformer.model.KeySettings.p1.left,
                platformer.model.KeySettings.p1.right,
                platformer.model.KeySettings.p1.enter,
                platformer.model.KeySettings.p1.cancel
        );
        overlayController.setKeyP2(
                platformer.model.KeySettings.p2.up,
                platformer.model.KeySettings.p2.down,
                platformer.model.KeySettings.p2.left,
                platformer.model.KeySettings.p2.right,
                platformer.model.KeySettings.p2.enter,
                platformer.model.KeySettings.p2.cancel
        );


    }

    private void setUpMapSelect() throws IOException {
        new Thread(new Task<>() {
            @Override
            protected Object call() throws Exception {
                loader = new FXMLLoader(getClass().getResource("/OverlayMapSelectMin.fxml"));
                Pane overlayMapSelectScreen = loader.load();

                overlayMapSelectController = loader.getController();
                selectStackPane.getChildren().add(overlayMapSelectScreen);
                return null;
            }
        }).run();

    }

    private void setUpAnnouncerCountDown() {
        for (int i =1;i<=10;i++) {
            announcerCountDown.put(Integer.toString(i),new Media(this.getClass().getResource(announcerFolder + i + ".wav").toString()));
        }

        announcerCountDown.put("TimeUp",new Media(this.getClass().getResource(announcerFolder+"Time Up.wav").toString()));
    }

    public void processKey(KeyCode key) {
        if (overlayController.allSelected) {
            overlayMapSelectController.processKey(key);
        }
        else {
            overlayController.processKey(key);
        }
    }

    public void fadeOutMusic() {
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.seconds(2),
                        new KeyValue(musicPlayer.volumeProperty(), 0)));
        timeline.play();
        timeline.setOnFinished(event -> {
            musicPlayer.dispose();
        });
    }

    public void startTimer() {
        if (timeline != null ) {
            timeline.stop();
        }

        timeSeconds.set(STARTTIME);
        timeline = new Timeline();
        timeline.getKeyFrames().add(
                new KeyFrame(Duration.seconds(STARTTIME+1),
                        new KeyValue(timeSeconds, 0)));
        timeline.playFromStart();


        timerLabel.textProperty().addListener((observable, oldValue, newValue) -> {
            int val = Integer.parseInt(newValue);
            if (overlayController.allSelected) {
                FadeTransition fade = new FadeTransition(Duration.millis(150),timerLabel);
                fade.setFromValue(1.0);
                fade.setToValue(0);
                fade.setDelay(Duration.millis(100));
                fade.play();
                timeline.stop();
            }
            if(val <= 10 && val >=0) {
                System.err.println("Time Left : "+newValue);
                {
                    MediaPlayer mp = new MediaPlayer(announcerCountDown.get((val == 0) ? "TimeUp" : newValue));
                    mp.play();
                    mp.setOnEndOfMedia(() -> mp.dispose());

                    if ( val == 0 ) {
                        if (!overlayController.allSelected) {
                            overlayController.timeupSelect();
                        }
                        FadeTransition fade = new FadeTransition(Duration.millis(150),timerLabel);
                        fade.setFromValue(1.0);
                        fade.setToValue(0);
                        fade.setDelay(Duration.millis(100));
                        fade.play();
                        timeline.stop();
                    }
                }
            }
        });
    }


    public TestCharacterSelectScreen() throws IOException, InterruptedException {
        announcerCountDown = new HashMap<String, Media>();
        selectView = new BorderPane();
        setUpAnnouncerCountDown();


        Media video = new Media(this.getClass().getResource("/testStageSelect.mp4").toString());
        MediaPlayer videoPlayer = new MediaPlayer(video);
        videoPlayer.setAutoPlay(true);
        videoPlayer.setCycleCount(MediaPlayer.INDEFINITE);
        videoBackground = new MediaView(videoPlayer);
        videoBackground.setFitHeight(720);
        videoBackground.setFitWidth(1280);

        selectStackPane = new StackPane();

        selectStackPane.getChildren().addAll(videoBackground,selectView);

        selectedCharacterOne = CharacterCollection.allCharacters.get("Random");
        selectedCharacterTwo = CharacterCollection.allCharacters.get("Random");


        setUpPlayerView();
        setUpMapSelect();

        final Font f = Font.loadFont(this.getClass().getResourceAsStream("/font/agency-fb_[allfont.net].ttf"),85);

        timerLabel.setText(timeSeconds.toString());
        timerLabel.textProperty().bind(timeSeconds.asString());
        timerLabel.setTextFill(Color.ORANGERED);
        timerLabel.setFont(f);

        timerLabel.setEffect(new DropShadow());

        selectStackPane.getChildren().add(timerLabel);


        //Set the stage's theme song
        musicFile = "04 select.mp3";
        //Need to improve loopBackPoint
        setUpMusic(0,31000,16173,true);

        //Add all necessary stuff to the stage itself
        {

            getChildren().addAll(selectStackPane);



        }
        startTimer();
    }


    public ArrayList<Character> getCharacterList() {
        playerCharacters = new ArrayList<>();
        playerCharacters.add(selectedCharacterOne);
        playerCharacters.add(selectedCharacterTwo);
        return playerCharacters;
    }


}
