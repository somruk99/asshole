package platformer.view;

import javafx.animation.*;
import javafx.beans.property.SimpleFloatProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;

import platformer.model.CharacterStats;

import java.util.Arrays;
import java.util.stream.Stream;

public class StatusView extends Pane {

    protected Label[] statLabels = {
            new Label("Health"),
            new Label("Attack"),
            new Label("Agility"),
            new Label("Speed")
    };


    protected ProgressBar healthBar = new ProgressBar();
    protected ProgressBar attackBar = new ProgressBar();
    protected ProgressBar agilityBar = new ProgressBar();
    protected ProgressBar speedBar  = new ProgressBar();

    private ProgressBar[]  statBars = {
            healthBar,attackBar,agilityBar,speedBar
    };

    private SimpleFloatProperty charHealth = new SimpleFloatProperty(0);
    private SimpleFloatProperty charAttack = new SimpleFloatProperty(0);
    private SimpleFloatProperty charAgility = new SimpleFloatProperty(0);
    private SimpleFloatProperty charSpeed = new SimpleFloatProperty(0);

    private SimpleFloatProperty[] charProprties = {
            charHealth,charAttack,charAgility,charSpeed
    };

    public Timeline createBarAnimations(SimpleFloatProperty property,float stat) {
        return new Timeline(
                new KeyFrame(
                        Duration.seconds(0.6),
                        new KeyValue(property, Math.abs(stat/100), Interpolator.EASE_IN)
                )
        );
    }

    public void rerenderStats(CharacterStats stats) {
        Timeline renderHealth = createBarAnimations(charHealth,stats.getHealth());
        Timeline renderAttack = createBarAnimations(charAttack,stats.getAttack());
        Timeline renderAgility = createBarAnimations(charAgility,stats.getAgility());
        Timeline renderSpeed = createBarAnimations(charSpeed,stats.getSpeed());
        Stream.of(renderHealth,renderAttack,renderAgility,renderSpeed).forEach(Animation::playFromStart);

        }


    public StatusView(CharacterStats stats) {
        final Font f = Font.loadFont(this.getClass().getResourceAsStream("/font/agency-fb_[allfont.net].ttf"),30);
        VBox statsBox = new VBox();
        statsBox.setPadding(new Insets(10));



        statBars[0].setStyle("-fx-accent: red;");
        statBars[1].setStyle("-fx-accent: orange;");
        statBars[2].setStyle("-fx-accent: blue;");
        //statBars[3].setStyle("-fx-accent: red;");
        Arrays.stream(statBars).forEach(progressBar -> progressBar.setPrefSize(120,15));

        for (int i=0, n=statBars.length; i<n;i++) {
            VBox statDisplay = new VBox();
            statBars[i].progressProperty().bind(charProprties[i]);

            statLabels[i].setFont(f);
            statLabels[i].setTextFill(Color.ALICEBLUE);
            statDisplay.getChildren().addAll(statLabels[i],statBars[i]);
            statDisplay.getStylesheets().add("/css/progressbar.css");
            statsBox.getChildren().add(statDisplay);
        }
        rerenderStats(stats);

        this.getChildren().add(statsBox);

    }

}
