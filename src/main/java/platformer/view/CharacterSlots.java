package platformer.view;

import javafx.animation.TranslateTransition;
import javafx.geometry.Insets;
import javafx.scene.effect.Glow;
import javafx.scene.effect.MotionBlur;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import platformer.model.Character;
import platformer.model.CharacterCollection;
/*import platformer.model.CharacterCollection;*/

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CharacterSlots extends Pane {
    //There will be 1 columns
    final int MAX_COLS = 0;
    //There will be 4 rows
    final int MAX_ROWS = 3;

    private int curCol;
    private int curRow;

    final int MIN_COLS = 0;
    final int MIN_ROWS = 0;

    private GridPane slots = new GridPane();

    private int player;

    private KeyCode upKey;
    private KeyCode downKey;
    private KeyCode leftKey;
    private KeyCode rightKey;

    public void setUpKey(KeyCode upKey) { this.upKey = upKey; }
    public void setDownKey(KeyCode downKey) { this.downKey = downKey; }
    public void setLeftKey(KeyCode leftKey) { this.leftKey = leftKey; }
    public void setRightKey(KeyCode rightKey) { this.rightKey = rightKey; }


    private Map<String,CharacterSlot> slotsMap = new HashMap<>();
    public CharacterSlot currentSelected;

    private void generateCharacterSlots(int colAmt, int rowAmt) {

        Character[] characters = CharacterCollection.allCharacters.values().toArray(new Character[CharacterCollection.allCharacters.size()]);

        CharacterSlot slot;
        for (int col = 0, maxCols = colAmt; col <= maxCols; col++) {
            for (int row = 0, maxRows = rowAmt; row <= maxRows; row++) {
                slot = new CharacterSlot(characters[row]);
                slotsMap.put(player+"c" + col + "r" + row, slot);
                slots.add(slot, col, row);
            }
        }
    }
    public void setSelected() {

            currentSelected.setEffect(new Glow());
            slots.setEffect(new MotionBlur());
            TranslateTransition tt = new TranslateTransition(Duration.millis(300), slots);
            tt.setFromX(0);
            tt.setToX((player == 1) ? -600 : 600);
            tt.play();
    }
    public void setUnselected() {
        currentSelected.setEffect(null);
        slots.setEffect(null);
        TranslateTransition tt = new TranslateTransition(Duration.millis(300),slots);
        tt.setFromX( (player == 1 )? -600:600 );
        tt.setToX(0);
        tt.play();
    }

    public void processKeyPress(KeyCode keyCode) {
        currentSelected.setUnselected();
        if (keyCode.equals(upKey)) {
            currentSelected = slotsMap.get(player+"c" + curCol + "r" +
                    (curRow = (curRow == MIN_ROWS) ? MAX_ROWS : curRow - 1));
        }
        if (keyCode.equals(downKey)) {
            currentSelected = slotsMap.get(player+"c" + curCol + "r" +
                    (curRow = (curRow == MAX_ROWS) ? MIN_ROWS : curRow + 1));
        }
        //Only 1 column, no need to traverse horizontally
        /*if (keyCode.equals(leftKey)) {
            currentSelected = slotsMap.get(player+"c" + (curCol = (curCol == MIN_COLS) ? MAX_COLS : curCol - 1) + "r" +
                    curRow);
        }
        if (keyCode.equals(rightKey)) {
            currentSelected = slotsMap.get(player+"c" + (curCol = (curCol == MAX_COLS) ? MIN_COLS : curCol + 1) + "r" +
                    curRow);
        }*/
        currentSelected.setSelected();
        if (player == 1) {
            TestCharacterSelectScreen.selectedCharacterOne = currentSelected.getCharacter();

        }
        if (player == 2) {
            TestCharacterSelectScreen.selectedCharacterTwo = currentSelected.getCharacter();
        }


    }


    public CharacterSlots(int player) throws IOException {
        this.player = player;
        slots.setPadding(new Insets(5,0,5,0));
        generateCharacterSlots(MAX_COLS,MAX_ROWS);
        if ( player == 1) {
            curCol = curRow = 0;
            currentSelected = slotsMap.get(player + "c0r0");
        }
        if ( player == 2) {
            curCol = 0;
            curRow = 0;
            currentSelected = slotsMap.get(player+"c0r0");
        }
        currentSelected.setSelected();

        this.getChildren().add(slots);
    }

}
