package platformer.view;

import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;
import platformer.controller.viewController.OverlayMapSelectController;
import platformer.model.Keys;

import java.io.IOException;


public class SelectScreen extends ViewBase {

        private final StackPane selectStackPane;
        private final MediaView videoBackground;
        private final BorderPane selectView;

        private FXMLLoader loader;

        public static OverlayMapSelectController overlayMapSelectController;

        private void setUpMapSelect() throws IOException {
            new Thread(new Task<>() {
                @Override
                protected Object call() throws Exception {
                    loader = new FXMLLoader(getClass().getResource("/OverlayMapSelectMin.fxml"));
                    return null;
                }
            }).run();
            Pane overlayMapSelectScreen = loader.load();

            overlayMapSelectController = loader.getController();
            selectStackPane.getChildren().add(overlayMapSelectScreen);
        }

        public void processKey(KeyCode key) {
                overlayMapSelectController.processKey(key);
        }
        public void fadeOutMusic() {
            Timeline timeline = new Timeline(
                    new KeyFrame(Duration.seconds(2),
                            new KeyValue(musicPlayer.volumeProperty(), 0)));
            timeline.play();
            timeline.setOnFinished(event -> {
            musicPlayer.dispose();
            });
        }


        public SelectScreen() throws IOException, InterruptedException {
            selectView = new BorderPane();


            Media video = new Media(this.getClass().getResource("/testStageSelect.mp4").toString());
            MediaPlayer videoPlayer = new MediaPlayer(video);
            videoPlayer.setAutoPlay(true);
            videoPlayer.setCycleCount(MediaPlayer.INDEFINITE);
            videoBackground = new MediaView(videoPlayer);
            videoBackground.setFitHeight(720);
            videoBackground.setFitWidth(1280);

            selectStackPane = new StackPane();

            selectStackPane.getChildren().addAll(videoBackground,selectView);

            setUpMapSelect();

            //Set the stage's theme song
            musicFile = "04 select.mp3";
            //Need to improve loopBackPoint
            setUpMusic(0,31000,16173,true);

            //Add all necessary stuff to the stage itself
            {
                getChildren().addAll(selectStackPane);
                overlayMapSelectController.setVisible();
            }
        }

}
