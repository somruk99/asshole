package platformer.view;

import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

public class ViewBase extends Pane {

    //Added from the original
    protected String musicFile;
    public Media music;
    public MediaPlayer musicPlayer;

    public void playMusic() {
        if (musicPlayer != null && music != null )
            musicPlayer.play();
    }

    protected void setUpMusic(long startPoint,long loopStartPoint,long loopBackPoint,boolean loop) {
        try {
            if (musicFile == null|| musicFile.isEmpty()) return;
            music = new Media(this.getClass().getResource("/sfx/"+musicFile).toString());
            musicPlayer = new MediaPlayer(music);
            //Set up when the music starts, should not be negative
            if (startPoint > 0) {
                musicPlayer.setStartTime(new Duration(startPoint));
            }
            //Set up when the music should loop, points should not be negative either
            if (loopStartPoint > 0 && loopBackPoint > 0 && loop) {
                //Set when the music enter its loop
                musicPlayer.setStopTime(new Duration(loopStartPoint));
                //Set loop back point after the music started playing
                musicPlayer.setOnPlaying(() -> musicPlayer.setStartTime(new Duration(loopBackPoint)));
            }
            //Set whether the song will replay on finished
            if (loop) {
                musicPlayer.setCycleCount(MediaPlayer.INDEFINITE);
            }
            musicPlayer.setAutoPlay(true);

        } catch (Exception e) {
            System.err.println("Failed to add a music for the stage!");
        }
    }


}
