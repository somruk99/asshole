import de.saxsys.javafx.test.JfxRunner;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import javafx.scene.input.KeyCode;

import platformer.controller.DrawingLoop;
import platformer.controller.GameLoop;
import platformer.model.CharacterCollection;
import platformer.view.Platform;
import platformer.model.Character;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

@RunWith(JfxRunner.class)
public class CharacterTest {
    private Character floatingCharacter;
    private int x = 30;
    private int y = 30;
    private int offsetX = 0;
    private int offsetY = 0;
    private KeyCode leftKey;
    private KeyCode rightKey;
    private KeyCode upKey;
    private Platform platformUnderTest;
    private GameLoop gameLoopUnderTest;
    private Method updateMethod;
    private Character onGroundCharacter;
    private DrawingLoop drawingLoopUnderTest;
    private Method checkDrawingCollisionMethod;
    private Method paintMethod;

    public CharacterTest() {
        this.leftKey = KeyCode.A;
        this.rightKey = KeyCode.D;
        this.upKey = KeyCode.W;
    }

    @Before
    public void setUp() throws NoSuchMethodException {
        this.floatingCharacter = CharacterCollection.allCharacters.get("Mario");
        this.onGroundCharacter = CharacterCollection.allCharacters.get("Batman");
        this.drawingLoopUnderTest = new DrawingLoop(this.platformUnderTest);
        this.checkDrawingCollisionMethod = DrawingLoop.class.getDeclaredMethod("checkDrawCollisions", ArrayList.class);
        this.checkDrawingCollisionMethod.setAccessible(true);
        this.paintMethod = DrawingLoop.class.getDeclaredMethod("paint", ArrayList.class);
        this.paintMethod.setAccessible(true);
        this.platformUnderTest = new Platform();
        this.gameLoopUnderTest = new GameLoop(this.platformUnderTest);
        this.updateMethod = GameLoop.class.getDeclaredMethod("update", ArrayList.class);
        this.updateMethod.setAccessible(true);
    }

    @Test
    public void whenConstructorIsCalledThenCharacterFieldsShouldMatchConstructorArguments() {
        Assert.assertEquals((double)this.x, (double)this.floatingCharacter.getX(), 0.0D);
        Assert.assertEquals((double)this.y, (double)this.floatingCharacter.getY(), 0.0D);
        Assert.assertEquals((double)this.offsetX, (double)this.floatingCharacter.getOffsetX(), 0.0D);
        Assert.assertEquals((double)this.offsetY, (double)this.floatingCharacter.getOffsetY(), 0.0D);
        Assert.assertEquals(this.leftKey, this.floatingCharacter.getLeftKey());
        Assert.assertEquals(this.rightKey, this.floatingCharacter.getRightKey());
        Assert.assertEquals(this.upKey, this.floatingCharacter.getUpKey());
    }

    @Test
    public void whenLeftKeyIsPressedThenCharacterMoveLeft() throws IllegalAccessException, InvocationTargetException {
        this.platformUnderTest.getKeys().add(this.leftKey);
        ArrayList<Character> c = new ArrayList();
        c.add(this.floatingCharacter);
        this.updateMethod.invoke(this.gameLoopUnderTest, c);
        Assert.assertEquals(true, this.platformUnderTest.getKeys().isPressed(this.leftKey));
        Assert.assertEquals(false, this.platformUnderTest.getKeys().isPressed(this.rightKey));
        Assert.assertEquals(true, this.floatingCharacter.isMoveLeft());
        Assert.assertEquals(false, this.floatingCharacter.isMoveRight());
        Assert.assertEquals(false, this.floatingCharacter.isCanJump());
    }

    @Test
    public void whenRightKeyIsPressedThenCharacterMoveRight() throws IllegalAccessException, InvocationTargetException {
        this.platformUnderTest.getKeys().add(this.rightKey);
        ArrayList<Character> c = new ArrayList();
        c.add(this.floatingCharacter);
        this.updateMethod.invoke(this.gameLoopUnderTest, c);
        Assert.assertEquals(false, this.platformUnderTest.getKeys().isPressed(this.leftKey));
        Assert.assertEquals(true, this.platformUnderTest.getKeys().isPressed(this.rightKey));
        Assert.assertEquals(false, this.floatingCharacter.isMoveLeft());
        Assert.assertEquals(true, this.floatingCharacter.isMoveRight());
        Assert.assertEquals(false, this.floatingCharacter.isCanJump());
    }

    @Test
    public void ifCharacterIsOnGroundAndUpKeyIsPressedThenCharacterJump() throws InvocationTargetException, IllegalAccessException {
        int start_y = this.onGroundCharacter.getY();
        ArrayList<Character> c = new ArrayList();
        c.add(this.onGroundCharacter);
        this.checkDrawingCollisionMethod.invoke(this.drawingLoopUnderTest, c);
        this.paintMethod.invoke(this.drawingLoopUnderTest, c);
        Assert.assertEquals(true, this.onGroundCharacter.isCanJump());
        this.platformUnderTest.getKeys().add(this.upKey);
        Assert.assertEquals(true, this.platformUnderTest.getKeys().isPressed(this.upKey));
        this.updateMethod.invoke(this.gameLoopUnderTest, c);
        this.paintMethod.invoke(this.drawingLoopUnderTest, c);
        Assert.assertEquals(false, this.onGroundCharacter.isCanJump());
        Assert.assertTrue(this.onGroundCharacter.getY() < start_y);
    }

    @Test
    public void ifCharacterIsNotOnGroundAndUpKeyIsPressedThenCharacterWillNotJump() throws InvocationTargetException, IllegalAccessException {
        int start_y = this.floatingCharacter.getY();
        ArrayList<Character> c = new ArrayList();
        c.add(this.floatingCharacter);
        this.checkDrawingCollisionMethod.invoke(this.drawingLoopUnderTest, c);
        this.paintMethod.invoke(this.drawingLoopUnderTest, c);
        Assert.assertEquals(false, this.onGroundCharacter.isCanJump());
        this.platformUnderTest.getKeys().add(this.upKey);
        Assert.assertEquals(true, this.platformUnderTest.getKeys().isPressed(this.upKey));
        this.updateMethod.invoke(this.gameLoopUnderTest, c);
        this.paintMethod.invoke(this.drawingLoopUnderTest, c);
        Assert.assertEquals(false, this.onGroundCharacter.isCanJump());
        Assert.assertTrue(this.onGroundCharacter.getY() > start_y);
    }
}
